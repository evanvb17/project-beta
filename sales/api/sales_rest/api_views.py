from django.shortcuts import render
from django.http import JsonResponse
import json
from .models import AutomobileVO, Salesperson, Customer, Sale
from django.views.decorators.http import require_http_methods
from .encoders import (
    AutomobileVODetailEncoder,
    SalespersonEncoder,
    CustomerEncoder,
    SalesEncoder,
)


@require_http_methods(["GET", "POST"])
def api_list_salesperson(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespersonEncoder,
        )
    else:
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(
            first_name=content["first_name"],
            last_name=content["last_name"],
            employee_id=content["employee_id"],
        )
        return JsonResponse(
            {"salesperson": salesperson},
            encoder=SalespersonEncoder,
        )


@require_http_methods(["DELETE", "GET"])
def api_show_salesperson(request, id):
    if request.method == "DELETE":
        try:
            count, _ = Salesperson.objects.get(id=id).delete()
            return JsonResponse({"deleted": count > 0}, status=200)
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"Response": "Salesperson does not exist"}, status=404
            )
    else:
        try:
            salesperson = Salesperson.objects.get(id=id)
            return JsonResponse(salesperson, encoder=SalespersonEncoder)
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"Response": "Salesperson does not exist"}, status=404
            )


@require_http_methods(["GET", "POST"])
def api_list_customer(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(
            first_name=content["first_name"],
            last_name=content["last_name"],
            address=content["address"],
            phone_number=content["phone_number"],
        )
        return JsonResponse(
            {"customer": customer},
            encoder=CustomerEncoder,
        )


@require_http_methods(["DELETE", "GET"])
def api_show_customer(request, id):
    if request.method == "DELETE":
        try:
            count, _ = Customer.objects.get(id=id).delete()
            return JsonResponse({"deleted": count > 0}, status=200)
        except Customer.DoesNotExist:
            return JsonResponse(
                {"Response": "Customer does not exist"}, status=404
            )
    else:
        try:
            customer = Customer.objects.get(id=id)
            return JsonResponse(customer, encoder=CustomerEncoder)
        except Customer.DoesNotExist:
            return JsonResponse(
                {"Response": "Customer does not exist"}, status=404
            )


@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SalesEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)

        try:
            automobile = AutomobileVO.objects.get(vin=content["automobile"])

            if automobile.sold:
                return JsonResponse(
                    {"error": "Automobile already sold"},
                    status=400,
                )
            automobile.sold = True
            try:

                salesperson = Salesperson.objects.get(id=content["salesperson"])
            except Salesperson.DoesNotExist:
                return JsonResponse(
                {"Response": "Salesperson does not exist"}, status=404
                )
            try:
                customer = Customer.objects.get(id=content["customer"])
            except Customer.DoesNotExist:
                return JsonResponse(
                 {"Response": "Customer does not exist"}, status=404
                )

            content['automobile'] = automobile
            content['salesperson'] = salesperson
            content['customer'] = customer

            sale = Sale.objects.create(**content)
            return JsonResponse(
                sale,
                encoder=SalesEncoder,
                safe=False,
            )
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"Response": "Automobile does not exist"}, status=404
            )


@require_http_methods(["DELETE", "GET"])
def api_show_sale(request, id):
    if request.method == "DELETE":
        try:
            count, _ = Sale.objects.get(id=id).delete()
            return JsonResponse({"deleted": count > 0}, status=200)
        except Sale.DoesNotExist:
            return JsonResponse(
                {"Response": "Sale does not exist"}, status=404
            )
    else:
        sale = Sale.objects.get(id=id)
        return JsonResponse(sale, encoder=SalesEncoder)
