import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { useState, useEffect } from 'react';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturersList from './inventory/ManufacturersList';
import ManufacturerForm from './inventory/ManufacturerForm';
import ModelsList from './inventory/ModelsList';
import ModelForm from './inventory/ModelForm';
import AutomobilesList from './inventory/AutomobilesList';
import AutomobileForm from './inventory/AutomobileForm';
import TechnicianForm from './service/TechnicianForm';
import TechnicianList from './service/TechnicianList';
import AppointmentForm from './service/AppointmentForm';
import AppointmentList from './service/AppointmentList';
import ServiceList from './service/ServiceHistory';
import SalesPeopleList from './sales/SalesPeopleList';
import SalesPersonForm from './sales/SalesPersonForm';
import CustomersList from './sales/CustomersList';
import CustomerForm from './sales/CustomerForm';
import SalesList from './sales/SalesList';
import SalesForm from './sales/SalesForm';
import SalesHistory from './sales/SalesHistory';

function App() {
  const [technicians, setTechnicians] = useState([])
  const [appointments, setAppointments] = useState([])

  const loadTechnicians = async () => {

    const response = await fetch("http://localhost:8080/api/technicians/")

    if (response.ok) {
      const techData = await response.json()

      setTechnicians(techData.technicians)

    }
  }

  const loadAppointments = async () => {

    const response = await fetch("http://localhost:8080/api/appointments/")

    if (response.ok) {
      const appointData = await response.json()

      setAppointments(appointData.appointments)

    }
  }

  useEffect( () => {
      loadTechnicians()
      loadAppointments()
  },[])



  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers">
            <Route index element={<ManufacturersList />} />
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path="models">
            <Route index element={<ModelsList />} />
            <Route path="new" element={<ModelForm />} />
          </Route>
          <Route path="automobiles">
            <Route index element={<AutomobilesList />} />
            <Route path="new" element={<AutomobileForm />} />
          </Route>
          <Route path="salespeople">
            <Route index element={<SalesPeopleList />} />
            <Route path="new" element={<SalesPersonForm />} />
          </Route>
          <Route path="customers">
            <Route index element={<CustomersList />} />
            <Route path="new" element={<CustomerForm />} />
          </Route>
          <Route path="sales">
            <Route index element={<SalesList />} />
            <Route path="new" element={<SalesForm />} />
            <Route path="history" element={<SalesHistory />} />
          </Route>
          <Route path="technician">
            <Route index element={<TechnicianList technicians={technicians} loadTechnicians={loadTechnicians}/>} />
            <Route path="new" element={<TechnicianForm />} />
          </Route>
          <Route path="appointment">
            <Route index element={<AppointmentList appointments={appointments} loadAppointments={loadAppointments}/>} />
            <Route path="new" element={<AppointmentForm />} />
            <Route path="history" element={<ServiceList appointments={appointments} loadAppointments={loadAppointments}/>} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
