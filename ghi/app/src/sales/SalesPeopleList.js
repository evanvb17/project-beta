import { useEffect, useState } from "react";

function SalesPeopleList() {
    const [salesPeople, setSalesPeople] = useState([]);

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/salespeople/');

        if (response.ok) {
            const data = await response.json();
            setSalesPeople(data.salespeople);
        }
    }

    useEffect(() => {
        getData()
    }, [])

    return (
        <>
        <div className="px-1 mt-2">
            <h2>Salespeople</h2>
        </div>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Employee ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                </tr>
            </thead>
            <tbody>
                {salesPeople.map(salesPeep => {
                    return (
                        <tr key={salesPeep.id}>
                            <td>{salesPeep.employee_id}</td>
                            <td>{salesPeep.first_name}</td>
                            <td>{salesPeep.last_name}</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>

    </>
    )
}

export default SalesPeopleList
