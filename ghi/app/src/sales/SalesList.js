import { useEffect, useState } from 'react';

function SalesHistory() {
    const [salesHistory, setSalesHistory] = useState([]);

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/sales/');

        if (response.ok) {
            const data = await response.json();
            setSalesHistory(data.sales);
        }
    }

    useEffect(() => {
        getData()
    }, [])
return (
    <>
    <div className="px-1 mt-2">
        <h2>Sales</h2>
    </div>
    <table className="table table-striped">
        <thead>
            <tr>
                <th>Salesperson Employee ID</th>
                <th>Salesperson Name</th>
                <th>Customer</th>
                <th>VIN</th>
                <th>Price</th>
            </tr>
        </thead>
        <tbody>
            {salesHistory.map(history => {
                return (
                   <tr key={history.id}>
                        <td>{history.salesperson.employee_id}</td>
                        <td>{history.salesperson.first_name} {history.salesperson.last_name}</td>
                        <td>{history.customer.first_name} {history.customer.last_name}</td>
                        <td>{history.automobile.vin}</td>
                        <td>${history.price}</td>
                    </tr>
                );
            })}
        </tbody>
    </table>

    </>
)
}

export default SalesHistory
