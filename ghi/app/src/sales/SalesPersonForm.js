import React, { useState } from "react";

function SalesPersonForm() {
  const [formData, setFormData] = useState({
    first_name: "",
    last_name: "",
    employee_id: "",
  });

  const handleSubmit = async (event) => {
    event.preventDefault();

    const salesPersonUrl = "http://localhost:8090/api/salespeople/";

    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(salesPersonUrl, fetchConfig);
    if (response.ok) {
      setFormData({
        first_name: "",
        last_name: "",
        employee_id: "",
      });
      window.location.href = "/salespeople";
    }
  };

  const handleChangeName = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,
      [inputName]: value,
    });
  };

  return (
    <div className="container my-5">
      <div className="row justify-content-center">
        <div className="col-sm-6">
          <div className="card shadow">
            <div className="card-body">
              <h1 className="card-title text-center mb-4">Add a Salesperson</h1>
              <form onSubmit={handleSubmit} id="create-salesperson-form">
                <div className="mb-3">
                  <input
                    onChange={handleChangeName}
                    required
                    placeholder="First name"
                    type="text"
                    name="first_name"
                    id="first_name"
                    className="form-control"
                  />
                </div>
                <div className="mb-3">
                  <input
                    onChange={handleChangeName}
                    required
                    placeholder="Last name"
                    type="text"
                    name="last_name"
                    id="last_name"
                    className="form-control"
                  />
                </div>
                <div className="mb-3">
                  <input
                    onChange={handleChangeName}
                    required
                    placeholder="Employee ID"
                    type="text"
                    name="employee_id"
                    id="employee_id"
                    className="form-control"
                  />
                </div>
                <div className="d-grid">
                  <button className="btn btn-lg btn-primary">Submit</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SalesPersonForm;
