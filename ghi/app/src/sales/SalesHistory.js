import React, { useEffect, useState } from 'react';

function SalesHistory() {
  const [sales, setSales] = useState([]);
  const [salesHuman, setSalesHuman] = useState([]);
  const [filterValue, setFilterValue] = useState('');

  const getPerson = async () => {
    const person = await fetch('http://localhost:8090/api/salespeople/');

    if (person.ok) {
      const personData = await person.json();
      setSalesHuman(personData.salespeople);
    }
  };

  useEffect(() => {
    getPerson();
  }, []);

  const getData = async () => {
    const response = await fetch('http://localhost:8090/api/sales/');

    if (response.ok) {
      const data = await response.json();
      setSales(data.sales);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  const handleFilterChange = (e) => {
    setFilterValue(e.target.value);
  };

  return (
    <>
      <div className="container-fluid my-2">
        <h2>Sales History</h2>
        <select
          name=""
          onChange={handleFilterChange}
          value={filterValue}
          className="form-select"
        >
          <option value="">All</option>
          {salesHuman.map((peep) => (
            <option key={peep.id} value={peep.id}>
              {peep.first_name} {peep.last_name}
            </option>
          ))}
        </select>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Salesperson</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {sales
            .filter((sale) => (filterValue ? sale.id == filterValue : true))
            .map((sale) => (
              <tr key={sale.id}>
                <td>
                  {sale.salesperson.first_name} {sale.salesperson.last_name}
                </td>
                <td>
                  {sale.customer.first_name} {sale.customer.last_name}
                </td>
                <td>{sale.automobile.vin}</td>
                <td>${sale.price}</td>
              </tr>
            ))}
        </tbody>
      </table>
    </>
  );
}

export default SalesHistory;
