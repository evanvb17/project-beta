import React, { useState, useEffect } from "react";

function SalesForm() {
    const [autos, setAutos] = useState([]);
    const [salesPerson, setSalesPerson] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [formData, setFormData] = useState({
        automobile: "",
        salesperson: "",
        customer: "",
        price: "",
    });

    const getSales = async () => {
        const response = await fetch("http://localhost:8100/api/automobiles/");

        if (response.ok) {
            const data = await response.json();
            setAutos(data.autos);
        }
    }

    useEffect(() => {
        getSales();
    }, []);

    const getSalesPerson = async () => {
        const response = await fetch("http://localhost:8090/api/salespeople/");

        if (response.ok) {
            const data = await response.json();
            setSalesPerson(data.salespeople);
        }
    }

    useEffect(() => {
        getSalesPerson();
    }, []);

    const getCustomer = async () => {
        const response = await fetch("http://localhost:8090/api/customers/");

        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers);
        }
    }

    useEffect(() => {
        getCustomer();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const salesUrl = "http://localhost:8090/api/sales/";

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": "application/json",
            },
        };
        console.log(formData)
        const response = await fetch(salesUrl, fetchConfig);

        if (response.ok) {
            setFormData({
                automobile: "",
                salesperson: "",
                customer: "",
                price: "",
            });
            window.location.href = "/sales";
        }
    }

    const handleChangeName = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    return (
        <>
        <div className="my-5 container-fluid">
            <div className="row justify-content-center">
                <div className="col col-sm-auto">
                    <div className="card shadow">
                        <div className="card-body">

                            <form onSubmit={handleSubmit} id="create-auto-form">
                                <h1 className="card-title">Record a new sale</h1>
                                <p className="mb-3">
                                    Automobile VIN.
                                </p>
                                <div className="mb-3">
                                    <select onChange={handleChangeName} required name="automobile" id="automobile" className="form-select">
                                        <option value="">Automobile VIN</option>
                                        {autos.map(auto => {
                                            return (
                                                <option key={auto.vin} value={auto.vin}>{auto.vin}</option>
                                            );
                                        })}
                                    </select>
                                    <select onChange={handleChangeName} required name="salesperson" id="salesperson" className="form-select">
                                        <option value="">Salesperson</option>
                                        {salesPerson.map(person => {
                                            return (
                                                <option key={person.id} value={person.id}>{person.first_name} {person.last_name}</option>
                                            );
                                        })}
                                    </select>
                                    <select onChange={handleChangeName} required name="customer" id="customer" className="form-select">
                                        <option value="">Customer</option>
                                        {customers.map(customer => {
                                            return (
                                                <option key={customer.id} value={customer.id}>{customer.first_name} {customer.last_name}</option>
                                            );
                                        })}
                                    </select>
                                    <input onChange={handleChangeName} required placeholder="Price" type="text" name="price" id="price" className="form-control" />
                                </div>
                                <button className="btn btn-lg btn-primary">Submit</button>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        </>
    )
}

export default SalesForm
