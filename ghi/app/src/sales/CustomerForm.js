import React, { useState, useEffect } from "react";

function CustomerForm() {
    const [formData, setFormData] = useState({
        first_name: "",
        last_name: "",
        phone_number: "",
        address: "",
    });

    const handleSubmit = async (event) => {
        event.preventDefault();

        const customerUrl = "http://localhost:8090/api/customers/";

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(customerUrl, fetchConfig);
        if (response.ok) {
            setFormData({
                first_name: "",
                last_name: "",
                phone_number: "",
                address: "",
            });
            window.location.href = "/customers";
        }
    };

    const handleChangeName = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    return (
        <div className="container my-5">
          <div className="row justify-content-center">
            <div className="col col-sm-auto">
              <div className="card shadow">
                <div className="card-body">
                  <form onSubmit={handleSubmit} id="create-customer-form">
                    <h1 className="card-title">Add a Customer</h1>
                    <div className="mb-3">
                      <input
                        onChange={handleChangeName}
                        required
                        placeholder="First name"
                        type="text"
                        name="first_name"
                        id="first_name"
                        className="form-control"
                      />
                    </div>
                    <div className="mb-3">
                      <input
                        onChange={handleChangeName}
                        required
                        placeholder="Last name"
                        type="text"
                        name="last_name"
                        id="last_name"
                        className="form-control"
                      />
                    </div>
                    <div className="mb-3">
                      <input
                        onChange={handleChangeName}
                        required
                        placeholder="Phone number"
                        type="text"
                        name="phone_number"
                        id="phone_number"
                        className="form-control"
                      />
                    </div>
                    <div className="mb-3">
                      <input
                        onChange={handleChangeName}
                        required
                        placeholder="Address"
                        type="text"
                        name="address"
                        id="address"
                        className="form-control"
                      />
                    </div>
                    <button className="btn btn-lg btn-primary">Submit</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    }

    export default CustomerForm;
