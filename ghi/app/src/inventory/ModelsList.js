import React, { useEffect, useState } from 'react';

function ModelsList() {
  const [models, setModels] = useState([]);

  const getData = async () => {
    const response = await fetch('http://localhost:8100/api/models/');

    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <div className="container my-5">
      <div className="px-1 mt-2">
        <h2>Models</h2>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Picture</th>
          </tr>
        </thead>
        <tbody>
          {models.map((model) => (
            <tr key={model.id}>
              <td className="align-middle">{model.name}</td>
              <td className="align-middle">{model.manufacturer.name}</td>
              <td>
                <img className="img-fluid w-25 height: auto p-3" src={model.picture_url} alt={model.name} />
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default ModelsList;
