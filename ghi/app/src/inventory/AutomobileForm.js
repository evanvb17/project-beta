import React, { useState, useEffect } from "react";

function AutomobileForm() {
  const [models, setModels] = useState([]);
  const [formData, setFormData] = useState({
    color: "",
    year: "",
    vin: "",
    model_id: "",
  });

  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    const response = await fetch("http://localhost:8100/api/models/");

    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    }
  };

  const handleChangeName = (e) => {
    const { name, value } = e.target;
    setFormData((prevFormData) => ({ ...prevFormData, [name]: value }));
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const autoUrl = "http://localhost:8100/api/automobiles/";

    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(autoUrl, fetchConfig);

    if (response.ok) {
      setFormData({
        color: "",
        year: "",
        vin: "",
        model_id: "",
      });
      window.location.href = "/automobiles";
    }
  };

  return (
    <div className="my-5 container-fluid">
      <div className="row justify-content-center">
        <div className="col col-sm-auto">
          <div className="card shadow">
            <div className="card-body">
              <h1 className="card-title">Create an Automobile</h1>
              <p className="mb-3">Please choose an Automobile.</p>
              <form onSubmit={handleSubmit} id="create-auto-form">
                <select
                  onChange={handleChangeName}
                  required
                  name="model_id"
                  id="model_id"
                  className="form-select mb-3"
                >
                  <option value="">Select a Model</option>
                  {models.map((model) => (
                    <option key={model.id} value={model.id}>
                      {model.name}
                    </option>
                  ))}
                </select>
                <input
                  onChange={handleChangeName}
                  required
                  placeholder="Color"
                  type="text"
                  name="color"
                  id="color"
                  className="form-control mb-3"
                />
                <input
                  onChange={handleChangeName}
                  required
                  placeholder="Year"
                  type="text"
                  name="year"
                  id="year"
                  className="form-control mb-3"
                />
                <input
                  onChange={handleChangeName}
                  required
                  placeholder="VIN"
                  type="text"
                  name="vin"
                  id="vin"
                  className="form-control mb-3"
                />
                <button className="btn btn-lg btn-primary">Submit</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default AutomobileForm;
