import React, { useState, useEffect } from "react";

function ModelForm() {
  const [manufacturers, setManufacturers] = useState([]);
  const [formData, setFormData] = useState({
    name: "",
    manufacturer_id: "",
    picture_url: "",
  });

  const getData = async () => {
    const response = await fetch("http://localhost:8100/api/manufacturers/");

    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const modelUrl = "http://localhost:8100/api/models/";

    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(modelUrl, fetchConfig);

    if (response.ok) {
      setFormData({
        name: "",
        manufacturer_id: "",
        picture_url: "",
      });
      window.location.href = "/models";
    }
  };

  const handleChangeName = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,
      [inputName]: value,
    });
  };

  return (
    <div className="my-5 container-fluid">
      <div className="row justify-content-center">
        <div className="col col-sm-auto">
          <div className="card shadow">
            <div className="card-body">
              <form onSubmit={handleSubmit} id="create-model-form">
                <h1 className="card-title">Create a Vehicle Model</h1>
                <p className="mb-3">Please choose a manufacturer.</p>
                <div className="mb-3">
                  <select
                    onChange={handleChangeName}
                    required
                    name="manufacturer_id"
                    id="manufacturer_id"
                    className="form-select"
                  >
                    <option value="">Select a manufacturer</option>
                    {manufacturers.map((manufacturer) => (
                      <option key={manufacturer.id} value={manufacturer.id}>
                        {manufacturer.name}
                      </option>
                    ))}
                  </select>
                  <input
                    onChange={handleChangeName}
                    required
                    placeholder="Picture URL"
                    type="url"
                    name="picture_url"
                    id="picture_url"
                    className="form-control"
                  />
                  <input
                    onChange={handleChangeName}
                    required
                    placeholder="Model name"
                    type="text"
                    name="name"
                    id="name"
                    className="form-control"
                  />
                </div>
                <button className="btn btn-lg btn-primary">Submit</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ModelForm;
