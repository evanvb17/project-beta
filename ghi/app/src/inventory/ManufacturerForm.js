import React, { useState } from "react";

function ManufacturerForm() {
    const [formData, setFormData] = useState({
        name: "",
    })

    const handleSubmit = async (event) => {
        event.preventDefault();

        const manufacturerUrl = "http://localhost:8100/api/manufacturers/";

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(manufacturerUrl, fetchConfig);
        if (response.ok) {
            setFormData({
                name: "",
            });
            window.location.href = "/manufacturers";
        }
    };

    const handleChangeName = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    };

    return (
        <div className="my-5 container-fluid">
            <div className="row justify-content-center">
                <div className="col col-sm-auto">
                    <div className="card shadow">
                        <div className="card-body">
                            <form onSubmit={handleSubmit} id="create-manufacturer-form">
                                <h1 className="card-title">Create a Manufacturer</h1>
                                <div className="mb-3">
                                    <input onChange={handleChangeName} required placeholder="Manufacturer name" type="text" name="name" id="name" className="form-control" />
                                </div>
                                <button className="btn btn-lg btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ManufacturerForm;
