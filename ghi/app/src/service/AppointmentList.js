import React, {useEffect, useState } from "react"

const handleCancel = async (item) => {
      const url = `http://localhost:8080/api/appointments/${item}/cancel/`

      const fetchConfig = {
        method: "PUT",
        body: {"status": "canceled"},
        headers: {
            "Content-Type": "application/json"
        }
      }

      const response = await fetch(url, fetchConfig)
      window.location.reload()
}



const handleFinish = async (item) => {
    const url = `http://localhost:8080/api/appointments/${item}/finish/`

      const fetchConfig = {
        method: "PUT",
        body: {"status": "finished"},
        headers: {
            "Content-Type": "application/json"
        }
      }

      const response = await fetch(url, fetchConfig)
      window.location.reload()
}












function Appointments(props) {
  if (props.appointments === undefined) {
    return null
    }
    return (
        <div>
    <h1>Service Appointments</h1>
    <table className="table table-striped">
    <thead>
      <tr>
        <th>VIN</th>
        <th>is VIP</th>
        <th>Customer</th>
        <th>Date</th>
        <th>Time</th>
        <th>Technician</th>
        <th>Reason</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      {props.appointments.map(appointment => {

        return (
          <tr key={appointment.id} >
            <td>{ appointment.vin }</td>
            <td>{ appointment.vip ? <>&#9989;</> : <>&#10060;</>} </td>
            <td>{ appointment.customer }</td>
            <td>{ new Date(appointment.date_time).toLocaleDateString("lookup") }</td>
            <td> {new Date(appointment.date_time).getHours()}:{new Date(appointment.date_time).getMinutes()}</td>
            <td>{ appointment.technician.first_name }</td>
            <td>{ appointment.reason }</td>
            <td>
                <button onClick={ () => handleCancel(appointment.id) } className="btn btn-danger">Cancel</button>
                <button onClick={ () => handleFinish(appointment.id) } className="btn btn-success">Finished</button>
            </td>
          </tr>
        );
      })}
    </tbody>
  </table>
  </div>
  )
}

export default Appointments
