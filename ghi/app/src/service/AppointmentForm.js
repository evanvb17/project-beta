import React, {useEffect, useState } from "react"


function AppointmentForm() {
    const [technicians, setTechnicians] = useState([])

    const [formData, setFormData] = useState( {
        vin:"",
        customer:"",
        date_time: "",
        technician: "",
        reason:""


    })

    const fetchData = async () => {
        const url = `http://localhost:8080/api/technicians/`
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setTechnicians(data.technicians)
        }
    }

    useEffect( () => {
        fetchData()
    }, [])


   const handleSubmit = async (e) => {
    e.preventDefault()

    const url = 'http://localhost:8080/api/appointments/'
    const fetchConfig = {
        method: "POST",
        body: JSON.stringify(formData),
        headers: {
            'Content-Type':'application/json',

        }
    }
    const response = await fetch(url, fetchConfig)

    if (response.ok) {
        setFormData({
            vin:"",
            customer:"",
            date_time: "",
            technician: "",
            reason:""
        })
        window.location.href="/appointment"
       }
   }

   const handleFormChange = (e) => {
        const value = e.target.value
        const inputName = e.target.name

        setFormData({
            ...formData,
            [inputName]: value
        })
   }


   return (
    <div className="row">
        <h1> Add an Appointment</h1>

        <form onSubmit={handleSubmit} id="create-appointment-form">
            <div className="form-floating mb-3">
                <input onChange={handleFormChange} placeholder="Automobile VIN" required type="text" name="vin" id="vin" className="form-control" />
                <label htmlFor="vin">Automobile VIN</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleFormChange} placeholder="Customer" required type="text" name="customer" id="customer" className="form-control" />
                <label htmlFor="customer">Customer</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleFormChange} placeholder="Date" required type="datetime-local" name="date_time" id="date_time" className="form-control" />
                <label htmlFor="date_time">Date</label>
            </div>

            <div className="mb-3">
                <select onChange={handleFormChange} placeholder="Technician" required name="technician" id="technician" className="form-select" >
                <option value="">Choose a technician</option>
                {technicians.map((technician) => {

                  return (
                    <option key={technician.id} value={technician.id}>{technician.first_name}</option>
                  )
                })}
                 </select>
            </div>
            <div className="mb-3">
              <label htmlFor="reason">Reason</label>
              <textarea onChange={handleFormChange} className="form-control" id="reason" rows="3" name="reason"></textarea>
            </div>

            <button className="btn btn-primary">Create</button>
        </form>
    </div>

   )
}

export default AppointmentForm
