import React, {useEffect, useState } from "react"

function TechnicianForm() {
    const [formData, setFormData] = useState( {
        first_name:"",
        last_name:"",
        employee_id: "",

    })

   const handleSubmit = async (e) => {
    e.preventDefault()

    const url = 'http://localhost:8080/api/technicians/'
    const fetchConfig = {
        method: "POST",
        body: JSON.stringify(formData),
        headers: {
            'Content-Type':'application/json',

        }
    }
    const response = await fetch(url, fetchConfig)

    if (response.ok) {
        setFormData({
            first_name:"",
            last_name:"",
            employee_id: "",
        })
        window.location.href="/technician"
    }
   }

   const handleFormChange = (e) => {
        const value = e.target.value
        const inputName = e.target.name

        setFormData({
            ...formData,
            [inputName]: value
        })
   }

   return (
    <div className="row">
        <h1> Add a Technician</h1>

        <form onSubmit={handleSubmit} id="create-technician-form">
            <div className="form-floating mb-3">
                <input onChange={handleFormChange} placeholder="First Name" required type="text" name="first_name" id="first_name" className="form-control" />
                <label htmlFor="first_name">First Name</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleFormChange} placeholder="Last Name" required type="text" name="last_name" id="last_name" className="form-control" />
                <label htmlFor="last_name">Last Name</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleFormChange} placeholder="Employee ID" required type="number" name="employee_id" id="employee_id" className="form-control" />
                <label htmlFor="employee_id">Employee ID</label>
            </div>
            <button className="btn btn-primary">Create</button>
        </form>
    </div>

   )
}

export default TechnicianForm
