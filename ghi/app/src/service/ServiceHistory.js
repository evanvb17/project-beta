import React, {useEffect, useState } from "react"


function ServiceHistory(props) {
    if (props.appointments === undefined) {
      return null
    }
    return (
        <div>
    <h1>Service History</h1>
    <table className="table table-striped">
    <thead>
      <tr>
        <th>VIN</th>
        <th>is VIP</th>
        <th>Customer</th>
        <th>Date</th>
        <th>Time</th>
        <th>Technician</th>
        <th>Reason</th>
        <th>Status</th>
      </tr>
    </thead>
    <tbody>
      {props.appointments.map(appointment => {

        return (
          <tr key={appointment.id} >
            <td>{ appointment.vin }</td>
            <td>{ appointment.vip ? <>&#9989;</> : <>&#10060;</> } </td>
            <td>{ appointment.customer }</td>
            <td>{ new Date(appointment.date_time).toLocaleDateString("lookup")}</td>
            <td>{ new Date(appointment.date_time).getHours()}:{new Date(appointment.date_time).getMinutes()}</td>
            <td>{ appointment.technician.first_name }</td>
            <td>{ appointment.reason }</td>
            <td>{ appointment.status } </td>
          </tr>
        );
      })}
    </tbody>
  </table>
  </div>
  )
}

export default ServiceHistory
