from django.urls import path
from .views import api_technicians, api_appointments, api_cancel, api_finish

urlpatterns = [
    path("technicians/", api_technicians, name="api_technicians"),
    path("appointments/", api_appointments, name="api_appointments"),
    path("technicians/<int:pk>/", api_technicians, name="api_technician"),
    path("appointments/<int:pk>/", api_appointments, name="api_appointment"),
    path("appointments/<int:pk>/cancel/", api_cancel, name="api_cancel"),
    path("appointments/<int:pk>/finish/", api_finish, name="api_finish"),

]
