from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from django.db.utils import IntegrityError
from django.db.models import ProtectedError

from common.json import ModelEncoder
from .models import Technician, AutomobileVO, Appointment

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold"
    ]

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name", "last_name", "employee_id","id"
    ]

class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [ "date_time",
         "reason",
        "status",
        "vin",
        "customer",
        "technician",
        "id",
        "vip"
    ]

    encoders = {
        "technician": TechnicianEncoder()
    }



@require_http_methods(["GET", "POST", "DELETE"])
def api_technicians(request, pk=None):
    if request.method == "GET":
        if pk is None:
            technicians = Technician.objects.all()
        else:
            try:
                technicians = Technician.objects.filter(id=pk)
            except id.DoesNotExist:
                return JsonResponse( {"message": "Invalid technician id number"}, status=400)
        return JsonResponse({ "technicians":technicians}, encoder=TechnicianEncoder, safe=False)
    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            technicians = Technician.objects.create(**content)

            return JsonResponse (
                technicians, encoder = TechnicianEncoder, safe= False
            )
        except IntegrityError:
            return JsonResponse( {"message": "Duplicate technician id number"}, status=400)
    else:
        try:
            try:
                count, _ = Technician.objects.get(id=pk).delete()
            except ProtectedError:
                return JsonResponse( {"message": "Cannot delete technician assigned to an appointment"}, status = 400)
        except Technician.DoesNotExist:
            return JsonResponse( {"message": "Invalid employee id"}, status = 400)
        return JsonResponse( {"deleted": count > 0})

@require_http_methods(["GET", "POST", "DELETE", "PUT"])
def api_appointments(request, pk=None):
    if request.method == "GET":
        try:

            if pk is None:
                appointments = Appointment.objects.all()

            else:
                try:
                    appointments = Appointment.objects.filter(id=pk)
                except Appointment.DoesNotExist:
                    return JsonResponse( {"message": "Invalid appointment id number"}, status=400)
        except AutomobileVO.DoesNotExist:
            return JsonResponse( {"message": "Invalid VIN number"}, status=400)

        return JsonResponse({ "appointments":appointments}, encoder=AppointmentEncoder, safe=False)
    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            technician = Technician.objects.get(id = content["technician"])
            content["technician"] = technician

            if AutomobileVO.objects.filter(vin=content["vin"]).exists():

                content['vip'] = True
            else:
                content['vip'] = False
            content["status"] = "created"
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Invalid employee id number"}, status=400)
        appointments = Appointment.objects.create(**content)

        return JsonResponse (
            appointments, encoder = AppointmentEncoder, safe= False
        )
    elif request.method == "DELETE":
        try:

            count, _ = Appointment.objects.get(id=pk).delete()

        except Appointment.DoesNotExist:
            return JsonResponse( {"message": "Invalid appointment id"}, status = 400)
        return JsonResponse( {"deleted": count > 0})

@require_http_methods(["PUT"])
def api_cancel(request, pk):
    appointment = Appointment.objects.get(id=pk)
    try:
        Appointment.objects.filter(id=pk).update(status="canceled")
        appointment = Appointment.objects.get(id=pk)
    except Appointment.DoesNotExist:
        return JsonResponse( {"message": "Invalid appointment id"}, status = 400)
    return JsonResponse(appointment, encoder = AppointmentEncoder, safe=False)

@require_http_methods(["PUT"])
def api_finish(request, pk):
    appointment = Appointment.objects.get(id=pk)
    try:
        Appointment.objects.filter(id=pk).update(status="finished")
        appointment = Appointment.objects.get(id=pk)
    except Appointment.DoesNotExist:
        return JsonResponse( {"message": "Invalid appointment id"}, status = 400)
    return JsonResponse(appointment, encoder = AppointmentEncoder, safe=False)
