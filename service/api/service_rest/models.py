from django.db import models
from django.urls import reverse

class Technician(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    employee_id = models.BigIntegerField(unique=True)


    def get_api_url(self):
        return reverse("api_technicians", kwargs={"pk": self.id})

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=200)
    sold = models.BooleanField(default = False)

    def __str__(self):
        return self.vin

class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.TextField()
    status = models.CharField(max_length=10, null=True, default="created")
    vin = models.CharField(max_length=200)
    customer = models.CharField(max_length=200)
    technician = models.ForeignKey(Technician, related_name="appointments", on_delete=models.PROTECT)
    vip  = models.BooleanField(default=False, null=True)

    def get_api_url(self):
        return reverse("api_appointments", kwargs={"pk": self.id})
